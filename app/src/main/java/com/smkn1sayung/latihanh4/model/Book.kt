package com.smkn1sayung.latihanh4.model

data class Book(
    var id : String?,
    var name : String?,
    var author : String?
)
