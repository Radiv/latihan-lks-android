package com.smkn1sayung.latihanh4.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.smkn1sayung.latihanh4.R
import com.smkn1sayung.latihanh4.model.Book

class BookAdapter(private val list : ArrayList<Book>): RecyclerView.Adapter<BookAdapter.BookViewHolder>() {

    class BookViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val txtJudul = itemView.findViewById<TextView>(R.id.txtJudul)
        private val txtAuthor = itemView.findViewById<TextView>(R.id.txtAuthor)

        fun bind(get: Book) {
            txtJudul.text = get.name
            txtAuthor.text = get.author
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        return BookViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_book, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        holder.bind(list[position])
    }
}