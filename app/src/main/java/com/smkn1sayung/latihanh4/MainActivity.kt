package com.smkn1sayung.latihanh4

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnLogin = findViewById<Button>(R.id.btnLogin)
        val btnsignUp = findViewById<Button>(R.id.btnSignUp)
        val txtEmail = findViewById<EditText>(R.id.txtEmail)
        val txtPassword = findViewById<EditText>(R.id.txtPassword)

        btnsignUp.setOnClickListener {
            val intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
        }

        btnLogin.setOnClickListener {
            Login(UserModel(null, txtEmail.text.toString(), txtPassword.text.toString()))
        }
    }

    fun Login(data: UserModel) {
        val handler = Handler(Looper.getMainLooper())
        val executor = Executors.newSingleThreadExecutor()

        val jsonObject = JSONObject()
        jsonObject.put("email", data.email)
        jsonObject.put("password", data.password)
        val jsonObjectString = jsonObject.toString()

        executor.execute {
            val url = URL("http://10.0.2.2:5000/Api/Auth")
            val client: HttpURLConnection

            try{
                client = url.openConnection() as HttpURLConnection
                client.requestMethod = "POST"
                client.setRequestProperty("Content-type", "application/json")
                client.setRequestProperty("Accept", "application/json")
                client.doInput = true
                client.doOutput = true
                val dataOutputStream = OutputStreamWriter(client.outputStream)
                dataOutputStream.write(jsonObjectString)
                dataOutputStream.flush()

                if (client.responseCode == HttpURLConnection.HTTP_OK) {
                    val response = client.inputStream.bufferedReader()
                        .use { it.readText() }  // defaults to UTF-8
                    handler.post {
                        val data = JSONObject(response)
                        val intent = Intent(this, HomeActivity::class.java)
                        intent.putExtra("token", data.getString("token"));
                        intent.putExtra("exp", data.getString("expired"));
                        startActivity(intent)
                        finish()
                    }
                } else {
                    handler.post {
                        Toast.makeText(this, "Data tidak ditemukan!", Toast.LENGTH_SHORT).show()
                    }
                }

            } catch (e: java.lang.Exception) {
                Log.e(ContentValues.TAG, "onCreate: ", e)
            }
        }
    }
}