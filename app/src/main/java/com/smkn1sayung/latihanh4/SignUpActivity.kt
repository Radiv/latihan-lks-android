package com.smkn1sayung.latihanh4

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import org.json.JSONObject
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.Executors

class SignUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        val nameText = findViewById<TextView>(R.id.txtName)
        val passText = findViewById<TextView>(R.id.txtPassword)
        val confText = findViewById<TextView>(R.id.txtConfirmPassword)
        val emailText = findViewById<TextView>(R.id.txtEmail)
        val btnRegis = findViewById<Button>(R.id.btnSignUp)

        btnRegis.setOnClickListener() {
            if (nameText.text.toString() == "" || passText.text.toString() == "" || confText.text.toString() == "" || emailText.text.toString() == "") {
                Toast.makeText(this, "Seluruh data wajib diisi", Toast.LENGTH_SHORT).show()
            } else {
                if (passText.text.toString() == confText.text.toString()) {
                    SignUp(UserModel(nameText.text.toString(), emailText.text.toString(), passText.text.toString()))
                } else {
                    Toast.makeText(this, "Password tidak sama", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    fun SignUp(data: UserModel) {
        val handler = Handler(Looper.getMainLooper())
        val executor = Executors.newSingleThreadExecutor()
        executor.execute {
            var url = URL("http://10.0.2.2:5000/Api/Users")
            val client: HttpURLConnection
            val jsonObject = JSONObject()
            jsonObject.put("name", data.name)
            jsonObject.put("email", data.email)
            jsonObject.put("password", data.password)
            val jsonObjectString = jsonObject.toString()

            try {
                client = url.openConnection() as HttpURLConnection
                client.requestMethod = "POST"
                client.setRequestProperty("Content-type", "application/json")
                client.setRequestProperty("Accept", "application/json")
                client.doInput = true
                client.doOutput = true
                val dataOutputStream = OutputStreamWriter(client.outputStream)
                dataOutputStream.write(jsonObjectString)
                dataOutputStream.flush()

                if (client.responseCode == HttpURLConnection.HTTP_OK) {
                    handler.post {
                        Toast.makeText(this, "Data Berhasil Disimpan", Toast.LENGTH_SHORT).show()
                    }
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    handler.post {
                        Toast.makeText(this, "Gagal Simpan Data", Toast.LENGTH_SHORT).show()
                    }
                }
            } catch (e: java.lang.Exception) {
                Log.e(ContentValues.TAG, "onCreate: ", e)
            }
        }
    }
}