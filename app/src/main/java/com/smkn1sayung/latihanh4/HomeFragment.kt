package com.smkn1sayung.latihanh4

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.SimpleAdapter
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.smkn1sayung.latihanh4.adapter.BookAdapter
import com.smkn1sayung.latihanh4.model.Book
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.util.concurrent.Executors

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var bookView: RecyclerView
    private lateinit var bookAdapter: BookAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)

        bookView = view.findViewById(R.id.rvBook)
        bookView.layoutManager = LinearLayoutManager(view.context)

        getBook()
        // Inflate the layout for this fragment
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    private fun getBook() {
        val handler = Handler(Looper.getMainLooper())
        val executor = Executors.newSingleThreadExecutor()
        var list = ArrayList<Book>()
        executor.execute {
            val url = URL("http://10.0.2.2:5000/Api/Book")
            val client: HttpURLConnection

            try {
                client = url.openConnection() as HttpURLConnection
                client.requestMethod = "GET"
                client.setRequestProperty("Accept", "application/json")
                client.setRequestProperty("Authorization", "Bearer "+ param1)
                client.doInput = true
                client.doOutput = false

                if (client.responseCode == HttpURLConnection.HTTP_OK) {
                    val response = client.inputStream.bufferedReader().use { it.readText() }
                    handler.post {
                        val data = JSONArray(response)
                        for (i in 0 until data.length()) {
                            val isi = JSONObject(data[i].toString())
                            list.add(Book( isi.getString("id"), isi.getString("name"), isi.getString("authors")))
                        }

                        bookAdapter = BookAdapter(list)
                        bookAdapter.notifyDataSetChanged()
                        bookView.adapter = bookAdapter
                    }
                }

            } catch (e: java.lang.Exception) {
                Log.e(ContentValues.TAG, "onCreate: ", e)
            }
        }
    }
}